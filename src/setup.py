from setuptools import setup, find_packages

setup(
    name='DynamicSubnet',
    version='0.1.0',
    description='Python module to find out available subnets',
    long_description='',
    keywords='subnet dynamic availablity',
    url='',
    author = 'Yohan VALETTE',
    author_email = 'yohan.valette@gmail.com',
    license='MIT',
    python_requires='>=2.7',
    install_requires=[
        'ipaddress'
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'dynamic-subnet = DynamicSubnet.script:main'
        ]
    },
    scripts=[]
)