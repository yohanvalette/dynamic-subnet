import unittest
from DynamicSubnet.library import Subnet, SubnetFinder


class DynamicSubnetTest(unittest.TestCase):

    def setUp(self):        
        pass

    def test_add_subnet(self):
        subnet_finder = SubnetFinder(address_space="10.1.0.0/16", subnets=[])

        count = subnet_finder.taken_subnets_count()
        self.assertEqual(0, count, "There should be no subnet")

        added = subnet_finder.add_subnet(Subnet("Subnet1", "10.1.0.0/18"))
        self.assertTrue(added, "The subnet should have been added")

        count = subnet_finder.taken_subnets_count()
        self.assertEqual(1, count, "There should be 1 subnet")

        added = subnet_finder.add_subnet(Subnet("Subnet1", "10.1.64.0/18"))
        self.assertFalse(
            added, "The subnet should have not been added because of duplicate name")

        added = subnet_finder.add_subnet(Subnet("Subnet2", "10.0.0.0/18"))
        self.assertFalse(
            added, "The subnet should have not been added because outside of address space")

    def test_overlap_existing(self):
        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=[
                Subnet("Subnet1", "10.1.0.0/19"),
                Subnet("Subnet2", "10.1.64.0/18"),
                # Free: 10.1.192.0/19
                Subnet("Subnet3", "10.1.224.0/19"),                
                ]
        )

        overlaps = subnet_finder.overlap_existing(cidr="10.1.0.0/17", ignore_subnet_name="Subnet1")
        self.assertTrue(overlaps, "The subnet should be overlapping 10.1.64.0/18")

        overlaps = subnet_finder.overlap_existing(cidr="10.1.32.0/19")
        self.assertFalse(overlaps, "The subnet should not be overlapping")

        overlaps = subnet_finder.overlap_existing("10.1.0.0/18", "Subnet1")
        self.assertFalse(overlaps, "The subnet should not be overlapping")
        
        overlaps = subnet_finder.overlap_existing("10.1.0.0/18")
        self.assertTrue(overlaps, "The subnet should be overlapping 10.1.0.0/19")

        overlaps = subnet_finder.overlap_existing("10.1.192.0/18")
        self.assertTrue(overlaps, "The subnet should be overlapping 10.1.224.0/19")

        overlaps = subnet_finder.overlap_existing("10.1.32.0/19")
        self.assertFalse(overlaps, "The subnet should NOT be overlapping")

    def test_name_exists(self):
        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=[Subnet("Subnet1", "10.1.128.0/19")]
        )

        exists = subnet_finder.name_exists("Subnet1")
        self.assertTrue(exists, "The name actually already exist")

        exists = subnet_finder.name_exists("Subnet2")
        self.assertFalse(exists, "The name does not actually exist")

    def test_get_subnet_by_name(self):
        raw_subnets = [
            {"name": "Subnet1", "cidr": "10.1.0.0/18"},
            {"name": "Subnet2", "cidr": "10.1.64.0/18"},
            {"name": "Subnet3", "cidr": "10.1.128.0/20"},
            {"name": "Subnet4", "cidr": "10.1.160.0/21"},
            {"name": "Subnet5", "cidr": "10.1.144.0/20"},
            {"name": "Subnet6", "cidr": "10.1.176.0/20"},
        ]
        subnets = []
        for subnet in raw_subnets:
            subnets.append(Subnet(subnet["name"], subnet["cidr"]))

        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=subnets
        )

        subnet = subnet_finder.get_subnet_by_name("SomeSubnet")
        self.assertIsNone(subnet, "The subnet should not be found")

        subnet = subnet_finder.get_subnet_by_name("Subnet1")
        self.assertIsNotNone(subnet, "The subnet should be found")
        self.assertEqual("10.1.0.0/18", str(subnet.cidr))

        subnet = subnet_finder.get_subnet_by_name("Subnet2")
        self.assertIsNotNone(subnet, "The subnet should be found")
        self.assertNotEqual("10.1.0.0/18", str(subnet.cidr))
        self.assertEqual("10.1.64.0/18", str(subnet.cidr))


    def test_list_network_cidr(self):
        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=[]
        )

        subnet_20 = list(subnet_finder.list_network_cidr(20))
        self.assertEqual(16, len(subnet_20), "They should have 16 subnets /20")

    # @unittest.skip("reason for skipping")
    def test_list_available_network_cidr(self):
        raw_subnets = [
            {"name": "Subnet1", "cidr": "10.1.0.0/18"},
            {"name": "Subnet2", "cidr": "10.1.64.0/18"},
            {"name": "Subnet3", "cidr": "10.1.128.0/20"},
            {"name": "Subnet4", "cidr": "10.1.160.0/21"},
            {"name": "Subnet5", "cidr": "10.1.144.0/20"},
            {"name": "Subnet6", "cidr": "10.1.176.0/20"},
        ]
        subnets = []
        for subnet in raw_subnets:
            subnets.append(Subnet(subnet["name"], subnet["cidr"]))

        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=subnets
        )

        subnet_20 = list(subnet_finder.list_available_network_cidr(20))
        self.assertEqual(4, len(subnet_20), "They should have 4 available subnets /20")

    def test_can_upsize_subnet(self):
        raw_subnets = [
            {"name": "Subnet1", "cidr": "10.1.0.0/19"},
            # Free: 10.1.32.0/19
            {"name": "Subnet2", "cidr": "10.1.64.0/18"},
            {"name": "Subnet3", "cidr": "10.1.224.0/20"},
            {"name": "Subnet4", "cidr": "10.1.240.0/20"},
        ]
        subnets = []
        for subnet in raw_subnets:
            subnets.append(Subnet(subnet["name"], subnet["cidr"]))

        subnet_finder = SubnetFinder(
            address_space="10.1.0.0/16",
            subnets=subnets
        )

        result = subnet_finder.can_upsize_subnet("Subnet1", 18)
        self.assertTrue(result, "Subnet should be able to be upsized to /18 (10.1.32.0/19 is free)")
        
        result = subnet_finder.can_upsize_subnet("Subnet1", 17)
        self.assertFalse(result, "Subnet should NOT be able to be upsized to /17 (overlap Subnet2)")

        result = subnet_finder.can_upsize_subnet("Subnet3", 19)
        self.assertFalse(result, "Subnet should not be able to be upsized to /20 (overlap Subnet4")

if __name__ == '__main__':
    unittest.main()
