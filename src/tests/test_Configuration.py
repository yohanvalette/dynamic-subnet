import unittest
import os
import sys
from DynamicSubnet.configuration import Configuration, ConfigurationMissingKeysException, ConfigurationInvalidFormatException


class ConfigurationTest(unittest.TestCase):
    def get_config(self, filename: str) -> str:
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)

    def setUp(self):
        pass

    def test__guess_format(self):
        filepath = self.get_config("01-config-valid.yml")
        configuration = Configuration(filepath)
        
        result = configuration._guess_format(filepath)
        self.assertIsNotNone(result)
        self.assertEqual("yaml", result, "Format should be yaml")

        filepath = self.get_config("01-config-valid.yaml")
        result = configuration._guess_format(filepath)
        self.assertIsNotNone(result)
        self.assertEqual("yaml", result, "Format should be yaml")
    
        filepath = self.get_config("01-config-valid.json")
        result = configuration._guess_format(filepath)
        self.assertIsNotNone(result)
        self.assertEqual("json", result, "Format should be json")

        filepath = self.get_config("01-config-valid.csv")
        self.assertRaises(ConfigurationInvalidFormatException, configuration._guess_format, filepath)

    def test__load_yaml(self):
        filepath = self.get_config("01-config-valid.yml")
        configuration = Configuration(filepath)
        
        self.assertEqual("10.1.0.0/16", configuration.network_address.with_prefixlen)
        self.assertEqual(6, len(configuration.subnets), "There should be 6 subnets in the configuration")
        self.assertEqual(0, len(configuration.invalid_subnets), "There should be 1 subnets in the configuration")
        self.assertFalse(configuration.has_config_error())

        filepath = self.get_config("unknown.yml")
        self.assertRaises(FileNotFoundError, configuration.load, filepath)

        filepath = self.get_config("02-config-valid.yml")
        configuration.load(filepath)        
        self.assertEqual(1, len(configuration.subnets), "There should be 1 subnets in the configuration")
        self.assertEqual(1, len(configuration.invalid_subnets), "There should be 1 invalid subnets in the configuration")
        self.assertTrue(configuration.has_config_error())


    def test__load(self):
        filepath = self.get_config("01-config-valid.json")
        configuration = Configuration(filepath)
        
        self.assertIsNotNone(configuration.network_address, "Network address should not be None")
        self.assertEqual("10.1.0.0/16", configuration.network_address.with_prefixlen)
        self.assertEqual(6, len(configuration.subnets), "There should be 6 subnets in the configuration")

        # Change file format
        filepath = self.get_config("02-config-valid.yml")
        configuration.load(filepath)        
        self.assertEqual(1, len(configuration.subnets), "There should be 1 subnets in the configuration")

    def test__load_json(self):
        filepath = self.get_config("01-config-valid.json")
        configuration = Configuration(filepath)
        
        self.assertIsNotNone(configuration.network_address, "Network address should not be None")
        self.assertEqual("10.1.0.0/16", configuration.network_address.with_prefixlen)
        self.assertEqual(6, len(configuration.subnets), "There should be 6 subnets in the configuration")

        filepath = self.get_config("unknown.json")
        self.assertRaises(FileNotFoundError, configuration.load, filepath)

    def test__load_yaml_invalid(self):
        filepath = self.get_config("03-config-invalid.yml")
        self.assertRaises(ConfigurationMissingKeysException, Configuration, filepath)

        filepath = self.get_config("04-config-invalid.yml")
        self.assertRaises(ConfigurationMissingKeysException, Configuration, filepath)

if __name__ == '__main__':
    unittest.main()
