from ipaddress import IPv4Network
import os
import yaml
import json
from yaml.loader import SafeLoader
from DynamicSubnet.library import Subnet

# Schema
#
# network_address:
# subnets_keys:
#  for_name: <name>
#  for_cidr: <cidr>
# subnets:
#  - <name>: subnet1
#    <cidr>: 10.1.1.0/16


class ConfigurationMissingKeysException(Exception):
    pass
class ConfigurationInvalidFormatException(Exception):
    pass

class Configuration:

    network_address: IPv4Network = None
    subnets: list[Subnet] = []
    invalid_subnets: list = []  # Subnets that could not be parsed

    def __init__(self, filepath: str) -> None:
        self.load(filepath)

    def load(self, filepath: str) -> bool:
        self._guess_format(filepath)
        self.config_file = filepath

        if self.config_format == "yaml":
            return self._load_yaml()
        elif self.config_format == "json":
            return self._load_json()
        else:
            return False

    def has_config_error(self):
        return len(self.invalid_subnets) != 0

    def _setup_config(self, config) -> bool:
        if not "network_address" in config:
            raise ConfigurationMissingKeysException(
                "The configuration file must contain a 'network_address' key")

        if not "subnets_keys" in config:
            raise ConfigurationMissingKeysException(
                "The configuration file must contain a 'subnets_keys' key")

        if not "for_name" in config["subnets_keys"] or not type(config["subnets_keys"]["for_name"]) is str:
            raise ConfigurationMissingKeysException(
                "The configuration file must contain a 'subnets_keys:for_name' key and it must be a string")

        if not "for_cidr" in config["subnets_keys"] or not type(config["subnets_keys"]["for_cidr"]) is str:
            raise ConfigurationMissingKeysException(
                "The configuration file must contain a 'subnets_keys:for_cidr' key")

        self.network_address = IPv4Network(config["network_address"])
        self.subnet_name_key = config["subnets_keys"]["for_name"]
        self.subnet_cidr_key = config["subnets_keys"]["for_cidr"]
        self.subnets = []
        self.invalid_subnets = []

        if "subnets" in config:
            for s in config["subnets"]:
                try:
                    self.subnets.append(
                        Subnet(name=s[self.subnet_name_key], cidr=s[self.subnet_cidr_key]))
                except Exception as e:
                    self.invalid_subnets.append({'entry': s, 'error': str(e)})
                    print(f"Warning: a subnet key was not found: {e}")

        return True

    def _load_json(self):
        with open(self.config_file) as f:
            config = json.load(f)
            return self._setup_config(config)

    def _load_yaml(self):
        with open(self.config_file) as f:
            self._setup_config(yaml.load(f, Loader=SafeLoader))

    def _guess_format(self, filepath: str) -> str:
        extension = os.path.splitext(filepath)[1]
        if extension == ".json":
            self.config_format = "json"
        elif extension == ".yml" or extension == ".yaml":
            self.config_format = "yaml"
        else:
            raise ConfigurationInvalidFormatException("The confugration must either json or yaml")

        return self.config_format

