from DynamicSubnet.library import Subnet, SubnetFinder
from DynamicSubnet.configuration import Configuration
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('action',
                    help='Valid actions: add-subnets, get-free-subnets, get-free-subnets')                  
parser.add_argument('--output-format',
                    help='Can be "yaml" or "json". Default "json"', default='json')
parser.add_argument('-c', '--config-file',
                    help='Path to the network configuration file. A json or a yaml file')
parser.add_argument('-o', '--out',
                    help='Output file', default='dynamic-subnet.json')
parser.add_argument('-p', '--prefix',
                    help='Output file', default='dynamic-subnet.json')                    
args = parser.parse_args()

# print 

def main():
    configuration=Configuration()
    subnet_finder = SubnetFinder(address_space=address_space, subnets=subnets)


if __name__ == '__main__':
    main()
