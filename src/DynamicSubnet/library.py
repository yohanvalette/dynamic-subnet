import yaml
import re
from yaml.loader import SafeLoader
from ipaddress import IPv4Network


class Subnet:
    name: str
    cidr: IPv4Network

    def __init__(self, name: str, cidr: str) -> None:
        self.name = name
        self.cidr = IPv4Network(cidr)

    def compare_prefix(self, new_prefix: int):
        if self.cidr.prefixlen == new_prefix:
            return 0
        elif self.cidr.prefixlen < new_prefix:
            return -1
        else:
            return 1

    def __repr__(self) -> str:
        return f"({self.name},{self.cidr})"


class SubnetFinder:

    def __init__(self, address_space: str, subnets: list[Subnet]) -> None:
        self.address_space = IPv4Network(address_space)
        self.subnets = []

        for subnet in subnets:
            tmp_sub = IPv4Network(subnet.cidr)
            self.add_subnet(subnet)

    def add_subnet(self, subnet: Subnet) -> bool:
        if subnet.cidr.subnet_of(self.address_space) and \
                not self.name_exists(name=subnet.name):
            self.subnets.append(subnet)
            return True
        else:
            return False

    def name_exists(self, name: str):
        return name in list(map(lambda s: s.name, self.subnets))

    def get_subnet_by_name(self, name: str):
        result = list(filter(lambda s: s.name == name, self.subnets))
        return None if len(result) == 0 else result[0]

    def overlap_existing(self, cidr: str, ignore_subnet_name: str = None):
        network = IPv4Network(cidr)
        for s in self.subnets:
            if network.overlaps(s.cidr) and (ignore_subnet_name == None or ignore_subnet_name != s.name):
                # print(f"{s.cidr} overlaps {network}")
                return True
        return False

    def taken_subnets_count(self):
        return len(self.subnets)

    def list_network_cidr(self, prefix: int):
        return self.address_space.subnets(new_prefix=prefix)

    def list_available_network_cidr(self, prefix: int):
        cidrs = self.list_network_cidr(prefix=prefix)

        available_cidr = []
        for cidr in cidrs:
            if not self.overlap_existing(cidr):
                available_cidr.append(cidr)

        return available_cidr

    def get_upsized_subnet(self, name: str, new_prefix: int):
        subnet = self.get_subnet_by_name(name)
        if subnet:
            return subnet.cidr.supernet(new_prefix=new_prefix)
        else:
            return None

    def can_upsize_subnet(self, name: str, new_prefix: int) -> bool:
        new_network = self.get_upsized_subnet(name, new_prefix)
        print(new_network)
        if new_network:
            subnet = self.get_subnet_by_name(name)
            return not self.overlap_existing(cidr=new_network, ignore_subnet_name=subnet.name)
        else:
            return False
